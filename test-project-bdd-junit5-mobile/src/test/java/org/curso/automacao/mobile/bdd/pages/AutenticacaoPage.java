package org.curso.automacao.mobile.bdd.pages;

import io.appium.java_client.pagefactory.AndroidFindBy;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.curso.automacao.mobile.bdd.Constants;
import org.curso.automacao.mobile.bdd.Hooks;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

@Getter
@NoArgsConstructor
public class AutenticacaoPage extends PageBase {

    public void validatePage(){
        validateElementPresence("//*[@resource-id='yourUsername']");
    }

    @AndroidFindBy(xpath = "//*[@resource-id='yourUsername']")
    private WebElement txtUser;

    @AndroidFindBy(xpath = "//*[@resource-id='yourPassword']")
    private WebElement txtPassword;

    @AndroidFindBy(xpath = "//*[@resource-id='logginButton']")
    private WebElement btnLogin;

}
