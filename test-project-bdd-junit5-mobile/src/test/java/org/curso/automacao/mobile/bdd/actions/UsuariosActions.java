package org.curso.automacao.mobile.bdd.actions;

import org.curso.automacao.mobile.bdd.pages.MasterPageFactory;
import org.curso.automacao.mobile.bdd.pages.UsuariosPage;

public class UsuariosActions {

    public static UsuariosPage usuariosPage() {
        return MasterPageFactory.getPage(UsuariosPage.class);
    }

    public static void validatePage(){
        usuariosPage().validatePage();
    }

    public static  void validateUserListLoadedSuccesfully(){
        usuariosPage().validateUserListLoadedSuccesfully();
    }

    public static void validateUserAlert(){
        usuariosPage().validateUserAlert();
    }

    public static void clickAlertOK(){
        usuariosPage().getBtnAlertOK().click();
    }

    public static void clickAlertaCancelar(){
        usuariosPage().getBtnAlertCancelar().click();
    }

    public static  void clickNewUser(){
        usuariosPage().getBtnNewUser().click();
    }

    public static void clickDeleteUser(){
        usuariosPage().getBtnDeleteUser1().click();
    }

    public static void clickUpdateUser(){
        usuariosPage().getBtnUpdateUser1().click();
    }

    public static void search(String value){
        usuariosPage().sendKeys(usuariosPage().getTxtSearch(), value);
    }

    public static void validateUserInList(String value){
        usuariosPage().validateUserInList(value);
    }

    public static  void validateUserDeleted(){
        usuariosPage().validateUserDeleted();
    }

    public static void deleteUser(){

        usuariosPage().swipeHorizontal();
        usuariosPage().swipeHorizontal();
        UsuariosActions.clickDeleteUser();
        UsuariosActions.validateUserAlert();
        UsuariosActions.clickAlertOK();

    }

}
