package org.curso.automacao.mobile.bdd.steps;

import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import lombok.extern.slf4j.Slf4j;
import org.curso.automacao.mobile.bdd.TempStorage;
import org.curso.automacao.mobile.bdd.actions.HomeActions;
import org.curso.automacao.mobile.bdd.actions.UsuariosActions;
import org.curso.automacao.mobile.bdd.actions.UsuariosCriarAlterarActions;

@Slf4j
public class UsuariosSteps {

    @Dado("que seja aberto o módulo de usuários")
    public void que_seja_aberto_o_módulo_de_usuários() {

        log.info("Abertura do módulo de usuários");
        HomeActions.clickMenuUsers(true);
        UsuariosActions.validatePage();
        UsuariosActions.validateUserListLoadedSuccesfully();

    }

    @Quando("for aberto o formulário de criação de um novo usuário")
    public void for_aberto_o_formulário_de_criação_de_um_novo_usuário() {

        log.info("Abertura do formulário de criação de usuário");

        UsuariosActions.clickNewUser();
        UsuariosCriarAlterarActions.validatePage();

    }

    @Quando("forem preenchidas os dados do cadastro do usuário")
    public void forem_preenchidas_os_dados_do_cadastro_do_usuário() {

        log.info("Preenchimento dos dados de cadastro do usuário");
        UsuariosCriarAlterarActions.fillForm();

    }

    @Quando("os dados do usuário forem submetidos")
    public void os_dados_do_usuário_forem_submetidos() {

        log.info("Submissão dos dados do usuário");
        UsuariosCriarAlterarActions.submit();

    }

    @Então("um novo usuário será criado com sucesso")
    public void um_novo_usuário_será_criado_com_sucesso() {

        log.info("Validação do novo usuário criado");
        UsuariosCriarAlterarActions.validateUserSavedSuccesfully();

    }

    @Quando("for realizada a busca de um usuário")
    public void for_realizada_a_busca_de_um_usuario(){

        log.info("Busca de um usuário");
        UsuariosActions.search(TempStorage.name );

    }

    @Então("o usuário deverá ser exibido com sucesso")
    public void o_usuario_devera_ser_exibido_com_sucesso(){

        log.info("Validação do resultado da busca");
        UsuariosActions.validateUserInList(TempStorage.name );

    }

    @Quando("for realizada a alteração do usuário")
    public void for_realizada_a_alteracao_do_usuario(){

        log.info("Realizando a alteração do usuário");
        UsuariosActions.clickUpdateUser();
        UsuariosCriarAlterarActions.validatePage();
        UsuariosCriarAlterarActions.validateUserLoadedSuccesfully(TempStorage.name );
        UsuariosCriarAlterarActions.updateUser();

    }

    @Então("a alteração deverá ser realizada com sucesso")
    public void a_alteracao_devera_ser_realizada_com_sucesso(){

        log.info("Validar a alteração do usuário.");
        UsuariosCriarAlterarActions.validateUserSavedSuccesfully();
    }

    @Quando("for realizada a exclusão do usuário")
    public void for_realizada_a_exclusão_do_usuario(){

        log.info("Realizar a exclusão do usuário");
        UsuariosActions.deleteUser();
    }

    @Então("a exclusão deverá ser realizada com sucesso")
    public void a_exclusao_deveroa_ser_realizada_com_sucesso(){

        log.info("Validar a exclusão do usuário");
        UsuariosActions.validateUserDeleted();
    }

}
