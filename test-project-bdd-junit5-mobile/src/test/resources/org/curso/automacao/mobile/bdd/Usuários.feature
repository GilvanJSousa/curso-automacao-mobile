#language:pt

  @usuarios
  Funcionalidade: Gerenciamento de Usuários no App
    Como usuário administrador do App
    Eu quero gerenciar os demais usuários
    Para que eles possam realizar transações

    Contexto: Autenticação de um usuário válido
      Dado que eu esteja na página de autenticação
      E faça o preenchimento dos dados válidos de autenticação com usuário "admin@automacao.org.br" e senha "password01"
      Quando submeter os dados de autenticação
      Então a página inicial será exibida

      @inserir
      Cenário: Cadastro de um novo usuário
        Dado que seja aberto o módulo de usuários
        Quando for aberto o formulário de criação de um novo usuário
        E forem preenchidas os dados do cadastro do usuário
        E os dados do usuário forem submetidos
        Então um novo usuário será criado com sucesso

      @buscar
      Cenário:  Busca de um usuário
        Dado que seja aberto o módulo de usuários
        Quando for realizada a busca de um usuário
        Então o usuário deverá ser exibido com sucesso

      @alterar
      Cenário: Alteração de um usuário
        Dado que seja aberto o módulo de usuários
        E for realizada a busca de um usuário
        E o usuário deverá ser exibido com sucesso
        Quando for realizada a alteração do usuário
        Então a alteração deverá ser realizada com sucesso

      @excluir
      Cenário: Exclusão de um usuário

        Dado que seja aberto o módulo de usuários
        E for realizada a busca de um usuário
        E o usuário deverá ser exibido com sucesso
        Quando for realizada a exclusão do usuário
        Então a exclusão deverá ser realizada com sucesso


