#language:pt

  @autenticacao
  Funcionalidade: Autenticação de Usuário no App
    Como usuário cadastrado e ativo no App
    Eu quero me autenticar
    Para poder realizar transações

    Esquema do Cenario: Autenticação de um usuário válido
      Dado que eu esteja na página de autenticação
      E faça o preenchimento dos dados válidos de autenticação com usuário "<usuário>" e senha "<senha>"
      Quando submeter os dados de autenticação
      Então a página inicial será exibida

      Exemplos:
        | usuário                   | senha               |
        | admin@automacao.org.br    | password01          |
